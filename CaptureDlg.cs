using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace UsbNi
{
    public partial class CaptureDlg : Form
    {
        public CaptureDlg()
        {
            InitializeComponent();
        }

        public CaptureDlg(TaskProperties TaskInfo)
        {
            InitializeComponent();
            TaskInfo.dlg = this;
            niTask.DoCapture(TaskInfo);
        }

        public void ShowSkipBtn()
        {
            BtnSkip.Visible = true;
            BtnCancel.Left = BtnSkip.Right + 10;
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            niTask.isUserCancelled = true;
            niTask.RamniStopCapture();
            DialogResult = DialogResult.Cancel;
            Close();
        }

        public void Message(string txt)
        {
            label1.Text = txt;
        }

        private void BtnSkip_Click(object sender, EventArgs e)
        {
            niTask.isUserSkipped = true;
            niTask.RamniStopCapture();
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }

    [Guid("78BB629C-35CF-4b7c-9D69-05DE6B7FCCB8")]
    public interface IDialogProperties
    {
        bool CenterDialog { get; set; }                  //1             \
        int CapX { get; set; }                            //0              > dialog window placement
        int CapY { get; set; }                            //0             /
        bool ShowDialog { get; set; }                   //TRUE          > want dialog? yes/no
        bool Success { get; set; }
        string Title { get; set; }
        bool ShowSkip { get; set; }
    }

    [Guid("C9CE4917-4A0C-4cb0-A19D-BC1726F6AA05")]
    public class DialogProperties : IDialogProperties
    {
        private bool _CenterDialog = true;                  //1             \
        private int _CapX = 512;                            //0              > dialog window placement
        private int _CapY = 384;                            //0             /
        private bool _ShowDialog = true;                    //TRUE          > want dialog? yes/no
        private bool _Success = true;
        private string _Title = "Capture Dialog";
        private bool _ShowSkip = false;

        public DialogProperties() {
            _CenterDialog = true;
            _CapX = 512;
            _CapY = 384;
            _ShowDialog = true;
            _Success = true;
            _Title = "Capture Dialog";
        }

        public bool CenterDialog {
            get { return _CenterDialog; }
            set { _CenterDialog = value; }
        }

        public int CapX {
            get { return _CapX; }
            set { _CapX = value; }
        }

        public int CapY {
            get { return _CapY; }
            set { _CapY = value; }
        }

        public bool ShowDialog {
            get { return _ShowDialog; }
            set { _ShowDialog = value; }
        }

        public bool Success {
            get { return _Success; }
            set { _Success = value; }
        }

        public string Title {
            get { return _Title; }
            set { _Title = value; }
        }

        public bool ShowSkip{
            get { return _ShowSkip; }
            set { _ShowSkip = value;}
        }
    }
}