using System;
using System.IO;
using NationalInstruments.DAQmx;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace UsbNi
{
    public delegate void ReportData(byte[] data);
    [Guid("D4ACC9E0-E6D8-4440-804D-74CDFAEAAFE1")]
    public interface ITaskProperties
    {
        string DeviceName { get; set; }
        string FileName { get; set; }        //fname
        int CapBlocks { get; set; }                               //capBlocks     > Capture length
        bool high_speed { get; set; }                         //FALSE         > speed lo/hi
        int timeout { get; set; }                             //20000         > time out 20 second default
        int MaxZeroBlocks { get; set; }                           //maxZeroBlocks > exit when exceeded
        bool debug { get; set; }
        bool saveGapFile { get; set; }
        bool completed { get; set; }
        bool error { get; set; }
        string errorMessage { get; set; }
        event ReportData ReportRecivedBytes;
        bool SendRawDataDelegate { get; set; }
    }

    [ClassInterface(ClassInterfaceType.None)]
    [Guid("63190643-7794-4504-A4AC-728EEB35AE1F")]
    public class TaskProperties : ITaskProperties
    {
        private string _DeviceName = "";
        private string _FileName = "c:\\working\\test.u1";        //fname
        private int _CapBlocks = 2;                               //capBlocks     > Capture length
        private bool _high_speed = false;                         //FALSE         > speed lo/hi
        private int _timeout = 20000;                             //20000         > time out 20 second default
        private int _MaxZeroBlocks = 4;                           //maxZeroBlocks > exit when exceeded
        private bool _debug = false;
        private bool _saveGapFile = false;
        private CaptureDlg _dlg = null;
        private bool _completed = false;
        private bool _error = false;
        private string _errorMessage = string.Empty;
        private bool _sendRawDataDelegate = false;
        private event ReportData _reportRecivedBytes = null;

        public TaskProperties() {
            _DeviceName = "";
            _FileName = "c:\\working\\test.u1";
            _CapBlocks = 2;
            _high_speed = false;
            _timeout = 20000;
            _MaxZeroBlocks = 4;
            _debug = false;
            _saveGapFile = false;
            _dlg = null;
            _completed = false;
            _error = false;
            _errorMessage = string.Empty;
        }

        public void SendDataToDelegate(byte[] data)
        {
            if (_reportRecivedBytes != null)
            {
                _reportRecivedBytes(data);
            }
        }

        public bool SendRawDataDelegate
        {
            get { return _sendRawDataDelegate; }
            set { _sendRawDataDelegate = value; }
        }

        public event ReportData ReportRecivedBytes
        {

            add
            {
                //lock (_reportRecivedBytes)
                {
                    _reportRecivedBytes += value;
                }
            }
            remove
            {
                //lock (_reportRecivedBytes)
                {
                    _reportRecivedBytes -= value;
                }

            }

        }

        public string DeviceName {
            get { return _DeviceName; }
            set { _DeviceName = value; }
        }

        public string FileName {
            get { return _FileName; }
            set { _FileName = value; }
        }

        public int CapBlocks {
            get { return _CapBlocks; }
            set { _CapBlocks = value; }
        }

        public bool high_speed {
            get { return _high_speed; }
            set { _high_speed = value; }
        }

        public int timeout {
            get { return _timeout; }
            set { _timeout = value; }
        }

        public int MaxZeroBlocks {
            get { return _MaxZeroBlocks; }
            set { _MaxZeroBlocks = value; }
        }

        public bool debug {
            get { return _debug; }
            set { _debug = value; }
        }

        public bool saveGapFile {
            get { return _saveGapFile; }
            set { _saveGapFile = value; }
        }

        public CaptureDlg dlg {
            get { return _dlg; }
            set { _dlg = value; }
        }

        public bool completed {
            get { return _completed; }
            set { _completed = value; }
        }

        public bool error {
            get { return _error; }
            set { _error = value; }
        }

        public string errorMessage {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }
    }

    public static class niTask
    {
        private const int LOW_SPEED  =  733333;
        private const int HIGH_SPEED = 1000000;

        private const string INTERNAL_CLOCK = "";
        private const string EXT_CLOCK_PORT = "PFI0";
        private const string EXT_CLOCK_SELECT = "/Port0/Line0";

        private static AnalogSingleChannelReader analogInReader;
        private static DigitalSingleChannelWriter DigitalWriter;
        private static Task DataInTask = null;
        private static Task SetPortTask = null;
        private static AsyncCallback analogCallback;    // 
        private static double[] data;                   // 
        private static int samplesPerChannel = 1000000;   // samples count buffer
        private static string PhysicalChannelInUse = "/ai0";
        private static FileStream oFile = null;         // output file stream
        private static byte Leftovers = 0;              // bits (< 8) at the end of a block to add to the next block
        private static int BitCount = 0;                // number of [Leftovers] bits
        private static int NumCapBlocks = 2;            // Number of (16k) blocks to capture, exit when reached
        private static int CaptureByteCount = 0;        // Total number of Bytes cpatured 
        private static int ZeroByteCount = 0;           // count 16K blocks of zero bytes
        private static bool Triggered = false;          // First non zero bit has been received?
        private static DateTime SignalTimeMark;         // Time mark of last signal received - used for time out
        private static double RoundPoint = 4.5;
        //Additional flags for synchronization and error handling
        public static bool enablequitconditions = true;
        public static bool isStarted = false;
        public static bool daqException = false;
        public static bool isUserCancelled = false;
        public static bool isUserSkipped = false;

        private static TaskProperties TaskInfo;

        public static void DoCapture(TaskProperties ti)
        {
            //Ensure no task is currently running
            RamniStopCapture();
            TaskInfo = ti;
            daqException = false;
            isUserCancelled = false;
            isUserSkipped = false;
            StartTask();            
        }

        public static void ResetTimer()
        {
            SignalTimeMark = DateTime.Now;
        }

        public static bool IsStarted()
        {
            if (TaskInfo != null)
            {
                if (TaskInfo.dlg != null)
                    return true;
            }
            return false;
        }

        public static void RamniStopCapture()
        {
            if (DataInTask != null)
            {
                DigitalWriter.WriteSingleSamplePort(true, 0);
                //Dispose of the task
                DataInTask.Dispose();
                SetPortTask.Dispose();
                DataInTask = null;
                oFile.Close();
                oFile = null;
            }
            if (TaskInfo != null)
            {
                TaskInfo.completed = true;
                if (TaskInfo.dlg != null)
                {
                    TaskInfo.dlg.Close();
                    if (TaskInfo.error)
                    {
                        TaskInfo.dlg.DialogResult = DialogResult.Cancel;
                    }
                    else
                        TaskInfo.dlg.DialogResult = DialogResult.OK;
                    TaskInfo.dlg = null;
                }
            }
            isStarted = false;
        }

        private static void EndTask(string msg)
        {
            RamniStopCapture();
            if (TaskInfo.debug)
                MessageBox.Show(msg, "USBNI");
            if (TaskInfo.error)
                TaskInfo.errorMessage = msg;
        }

        private static void StartTask()
        {
            ZeroByteCount = 0;
            CaptureByteCount = 0;
            if (DataInTask == null)
            {
                try
                {
                    //Create a new task
                    DataInTask = new Task();
                    SetPortTask = new Task();

                    //Create a virtual channel
                    DataInTask.AIChannels.CreateVoltageChannel(TaskInfo.DeviceName +
                                                    PhysicalChannelInUse, "DataInput",
                                                    AITerminalConfiguration.Rse, 0, 5, 
                                                    AIVoltageUnits.Volts);
                    SetPortTask.DOChannels.CreateChannel(TaskInfo.DeviceName + EXT_CLOCK_SELECT,
                                                    "ClkSelect",
                                                    ChannelLineGrouping.OneChannelForEachLine);

                    //Configure the timing parameters
                    double RateValue = Convert.ToDouble((TaskInfo.high_speed)?HIGH_SPEED:LOW_SPEED);
                    string ClockSource = EXT_CLOCK_PORT;        //INTERNAL_CLOCK;

                    DataInTask.Timing.ConfigureSampleClock(
                            ClockSource, 
                            RateValue, 
                            SampleClockActiveEdge.Rising, 
                            SampleQuantityMode.ContinuousSamples, 
                            samplesPerChannel);

                    //Verify the Task
                    DataInTask.Control(TaskAction.Verify);
                    SetPortTask.Control(TaskAction.Verify);

                    //SetPortTask.Control(TaskAction.Start);
                    DigitalWriter = new DigitalSingleChannelWriter(SetPortTask.Stream);
                    DigitalWriter.WriteSingleSamplePort(true, 0);
                    if (TaskInfo.high_speed)
                    {
                        DigitalWriter.WriteSingleSamplePort(true, 1);
                    }

                    analogInReader = new AnalogSingleChannelReader(DataInTask.Stream);
                    DataInTask.Stream.ReadWaitMode = ReadWaitMode.Poll;

                    //For .NET Framework 2.0, use SynchronizeCallbacks to specify that the object 
                    //marshals callbacks across threads appropriately.
                    analogInReader.SynchronizeCallbacks = true;

                    BitCount = 0;
                    Triggered = false;
                    SignalTimeMark = DateTime.Now;

                    oFile = File.Open(TaskInfo.FileName, FileMode.Create, FileAccess.Write);
                    analogCallback = new AsyncCallback(AnalogInCallback);
                    analogInReader.BeginReadMultiSample(-1, analogCallback, null);

                }
                catch (DaqException exception)
                {
                    //Display Errors
                    daqException = true;
                    RamniStopCapture();
                    throw new Exception(exception.Message);
                }
            }
            else
            {
                //Dispose of the task
                EndTask("Normal End");
            }
        }

        private static void AnalogInCallback(IAsyncResult ar)
        {
            try
            {
                if (DataInTask != null)
                {
                    //Read the available data from the channels
                    data = analogInReader.EndReadMultiSample(ar);
                    if (data.Length > 0)
                    {
                        dataToFile(data);
                    }
                    if (!TaskInfo.completed)
                        analogInReader.BeginReadMultiSample(-1, analogCallback, null);
                }
            }
            catch (DaqException exception)
            {
                //Display Errors
                TaskInfo.error = true;
                daqException = true;
                EndTask(exception.Message);
                throw new Exception(exception.Message);
            }
        }

        private static void dataToFile(double[] sourceArray)
        {
            try
            {
                int dataCount = (sourceArray.GetLength(0));
                byte[] bt = new byte[(dataCount / 8) + 2];

                if (BitCount > 0)
                    bt[0] = Leftovers;

                bool AllZeros = ConvertToByteArray(sourceArray, bt);

                if (AllZeros)
                {
                    if (Triggered)
                        ZeroByteCount += bt.Length;
                    if(!isStarted)
                        isStarted = true;
                }
                else
                {
                    Triggered = true;
                }

                int ByteCount = (((BitCount % 8) > 0) ? BitCount - 1 : BitCount) / 8;

                if (Triggered)
                {
                    CaptureByteCount += bt.Length;
                    if (TaskInfo.SendRawDataDelegate)
                    {
                        TaskInfo.SendDataToDelegate(bt);
                    }
                    else
                    {
                        if (oFile != null)
                        {
                            oFile.Write(bt, 0, ByteCount);
                            oFile.Flush();
                        }
                    }
                    //oFile.Write(bt, 0, ByteCount);
                }
                Leftovers = bt[(BitCount / 8)];
                BitCount = BitCount % 8;

                if (enablequitconditions)
                CheckQuitConditions();
            }
            catch (Exception e)
            {
                string Msg = string.Format("dataToFile error: {0}", e.TargetSite.ToString());
                TaskInfo.error = true;
                EndTask(Msg);
            }
        }

        /// <summary>
        /// convert the array of voltage levels to an array of bytes 
        /// each voltage reading is one bit
        /// NOTE: The captured signal is inverted so it's corrected here.
        /// </summary>
        /// <param name="sourceArray"></param>
        /// <param name="bt"></param>
        /// <returns></returns>
        private static bool ConvertToByteArray(double[] sourceArray, byte[] bt)
        {
            int dataCount = (sourceArray.GetLength(0));
            bool AllZeros = true;

            int ByteNum = 0;
            for (int i = 0; i < dataCount; i++)
            {
                ByteNum = BitCount / 8;
                double volts = sourceArray[i];
                bt[ByteNum] <<= 1;
                bt[ByteNum] |= (byte)((volts > RoundPoint) ? 0 : 1);    // invert!

                if (AllZeros && bt[ByteNum] > 0)
                    AllZeros = false;

                if (Triggered || !AllZeros)
                    BitCount++;
            }
            if (!AllZeros)
                ZeroByteCount = 0;

            return AllZeros;
        }

        private static void CheckQuitConditions()
        {
            if (ZeroByteCount > (TaskInfo.MaxZeroBlocks * 0x4000))
            {
                TaskInfo.error = false;
                EndTask("zero count");
                return;
            }

            if (TaskInfo.CapBlocks > 0 &&
                CaptureByteCount > (TaskInfo.CapBlocks * 0x4000))
            {
                TaskInfo.error = false;
                EndTask("block count");
                return;
            }

            TimeSpan TimeSinceLastSignal = ( DateTime.Now - SignalTimeMark );
            if (TimeSinceLastSignal.TotalMilliseconds > TaskInfo.timeout)
            {
                TaskInfo.error = true;
                EndTask("time out");
                return;
            }

        }
    }
}
