using System.Drawing;
using System.Windows.Forms;
using NationalInstruments.DAQmx;
using System.Runtime.InteropServices;

namespace UsbNi
{
    /// <summary>
    /// Access point for UsbNi
    /// </summary>
    [Guid("28FC67FD-ECE5-42df-A819-74D22C5D4B86")]
    public interface IUSBRamni
    {
        bool UsbDoCapture(int lcap, int timeout, int maxZeroBlocks, string filePath, bool saveGaps);
        bool DoCapture(IDialogProperties dp, ITaskProperties tp);
        bool StartUSBTask(int lcap, int timeout, int maxZeroBlocks, string filePath, bool saveGaps);
        void StopUSBTask();
        bool ShowDialog(bool CenterDialog, int CapX, int CapY);
        bool FindUSBCaptureDevice();
        void EnableQuitConditionsCheck(bool enable);
        bool IsCaptureStarted();
        bool IsDaqException();
        bool IsUserCancelled();
        bool IsUserSkipped();
    }

    [ClassInterface(ClassInterfaceType.None)]
    [Guid("B59A57A8-3505-4625-AA7B-BA1B47343879")]
    public class USBRamni : UsbNi.IUSBRamni
    {
        private static CaptureDlg CaptDlg = null;
        private static string devicename = "";
        TaskProperties tp = null;
        DialogProperties dp = null;

        /// <summary>
        /// constructor
        /// </summary>
        public USBRamni()
        {
            devicename = "";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lcap"></param>
        /// <param name="timeout"></param>
        /// <param name="maxZeroBlocks"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public bool UsbDoCapture(int capBlocks, int timeout, int maxZeroBlocks, string filePath, bool saveGaps)
        {
            dp = new UsbNi.DialogProperties();
            tp = new UsbNi.TaskProperties();
            tp.CapBlocks = capBlocks;
            tp.timeout = timeout;
            tp.MaxZeroBlocks = maxZeroBlocks;
            tp.FileName = filePath;
            tp.saveGapFile = saveGaps;
            return DoCapture(dp, tp);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dp"></param>
        /// <param name="tp"></param>
        /// <returns></returns>
        public bool DoCapture(IDialogProperties dp, ITaskProperties tp)
        {
            DialogProperties dialogProperties = (DialogProperties)dp;
            TaskProperties taskProperties = (TaskProperties)tp;

            bool result = true;
            if (!VerifyDevice(taskProperties))
                return false;
            if (dp.ShowDialog)
                result = CaptureWithDialog(dialogProperties, taskProperties) == DialogResult.OK;
            else
                niTask.DoCapture(taskProperties);
            return result;
        }

        public bool StartUSBTask(int capBlocks, int timeout, int maxZeroBlocks, string filePath, bool saveGaps)
        {
            tp = new UsbNi.TaskProperties();
            if (!VerifyDevice(tp))
                return false;
            tp.CapBlocks = capBlocks;
            tp.timeout = timeout;
            tp.MaxZeroBlocks = maxZeroBlocks;
            tp.FileName = filePath;
            tp.saveGapFile = saveGaps;
            niTask.DoCapture(tp);
            return tp.error;
        }

        public void StopUSBTask()
        {
            niTask.RamniStopCapture();
        }

        public bool ShowDialog(bool CenterDialog, int CapX, int CapY)
        {
            if (tp != null && !tp.completed && !tp.error)
            {
                dp = new UsbNi.DialogProperties();
                CaptDlg = new CaptureDlg();
                SetDialogPosition(dp);
                tp.dlg = CaptDlg;
                CaptDlg.ShowDialog();
                return !tp.error;
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dp"></param>
        /// <param name="tp"></param>
        private DialogResult CaptureWithDialog(DialogProperties dp, TaskProperties tp)
        {
            CaptDlg = new CaptureDlg(tp);

            SetDialogPosition(dp);
            CaptDlg.Text = dp.Title;

            CaptDlg.ShowDialog();
            return CaptDlg.DialogResult;
        }

        private static void SetDialogPosition(DialogProperties dp)
        {
            int h = CaptDlg.Height + 30;            // client area + frame                
            CaptDlg.Location = new Point(dp.CapX, dp.CapY);
            CaptDlg.Height = h;
            if(dp.ShowSkip)
                CaptDlg.ShowSkipBtn();
            if (dp.CenterDialog)
                CaptDlg.StartPosition = FormStartPosition.CenterScreen;
            else
                CaptDlg.StartPosition = FormStartPosition.Manual;
        }

        private bool VerifyDevice(TaskProperties tp)
        {
            if (string.IsNullOrEmpty(tp.DeviceName))
            {
                if (!FindUSBCaptureDevice())
                {
                    return false;
                }
                tp.DeviceName = devicename;
            }
            return true;
        }

        /// <summary>
        /// Find the USB capture device
        /// </summary>
        /// <returns>true if found, false if not</returns>
        public bool FindUSBCaptureDevice()
        {
            string[] list = DaqSystem.Local.GetTerminals(TerminalTypes.Basic);
            foreach (string terminal in list)
            {
                if (terminal.EndsWith("/di/SampleClock"))    // "/Dev2/di/SampleClock" ?
                {
                    devicename = terminal.Substring(1, 4);
                    break;
                }
            }

            return !string.IsNullOrEmpty(devicename);
        }

        public void EnableQuitConditionsCheck(bool enable)
        {
            niTask.enablequitconditions = enable;
            if (enable)
                niTask.ResetTimer();
        }

        public bool IsCaptureStarted()
        {
            return niTask.isStarted;
        }

        public bool IsDaqException()
        {
            return niTask.daqException;
        }

        public bool IsUserCancelled()
        {
            return niTask.isUserCancelled;
        }
        
        public bool IsUserSkipped()
        {
            return niTask.isUserSkipped;
        }

    }
}
